cases = [
    {
        'inputs': {
            'val': ''
        },
        'outputs': False,
        'description': "Empty string is not a valid vin"
    },
    {
        'inputs': {
            'val': True
        },
        'outputs': False,
        'description': "Must be string typed"
    },
    {
        'inputs': {
            'val': '123512'
        },
        'outputs': False,
        'description': "Expected 17 characters"
    },

    {
        'inputs': {
            'val': '12345678901234567'
        },
        'outputs': False,
        'description': "Does not match check digit validation"
    },
    {
        'inputs': {
            'val': 'IM8GDM9AXKP042788'
        },
        'outputs': False,
        'description': "Illegal char"
    },

    {
        'inputs': {
            'val': 'IM8GDM9AXUU042788'
        },
        'outputs': False,
        'description': "Illegal char in index 10"
    },
    {
        'inputs': {
            'val': '1M8GDM9AXKP042788'
        },
        'outputs': True,
        'description': "Valid VIN"
    },
]

cases_vehicle_year = [
    {
        'inputs': {
            'vin':  '1ZVFT80N465228314'
        },
        'outputs': 2006
    }
]

cases_vehicle_manufacturer = [
    {
        'inputs': {
            'vin':  '1ZVFT80N465228314'
        },
        'outputs': 'FORD'
    },
    {
        'inputs': {
            'vin':  '1ZVFT'
        },
        'outputs': 'FORD'
    },
    {
        'inputs': {
            'vin':  '1G1ZS58F47F170546'
        },
        'outputs': 'CHEV'
    }

]