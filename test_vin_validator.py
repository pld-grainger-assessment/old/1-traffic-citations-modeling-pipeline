import pytest
from tests.utils.test_case_runner import Test_Case_Runner
from utils.vin_validator import VIN
from tests.cases.vin_validator import cases_vehicle_manufacturer

class Test_Results(Test_Case_Runner):
    @pytest.mark.parametrize("case", cases_vehicle_manufacturer)
    def test_manufacturer(self, case):
        '''
        Run the tests for this question against the main method

        case: dict<str:Any>: The case to test
        '''
        print(case)
        model = VIN(**case.get('inputs', {}))
        result = model.manufacturer
        assert result == case.get('outputs')