import pandas as pd
from utils.conversions import try_to_convert_to_int
from utils.vin_validator import VIN
import json
import numpy as np

def clean_df(df, popular_makes, popular_violation_codes, numeric_features, category_to_int_maps,
             should_add_label=False, path_to_project_root='./', **kwargs):
    '''
    Perform necessary cleaning before passing to pipeline

    NOTES:
        - Little inconsistent in loading known vins, but passing everything else in..

    :param df: The dataframe to prep for the pipeline
    :param popular_makes: list<str>: The most popular makes from the uncorrupted dataset
    :param popular_violation_codes: list<str> The most popular violation codes
    :param numeric_features: list<str>: The columns to treat as numeric features
    :param category_to_int_maps: dict<str, dict<str, int>>: The columns to treat as categorical with the mappings of the categorical values to unique integer values (Needed for one-hot encoding in the Pipeline)
    :param should_add_label: bool: Whether to add the label column.  Only needed for training.  Not needed for predictions.
    :param path_to_project_root: str: The path from the file loading this method to the root of the project. Needed to load the known vins
    :return: pd.Dataframe: The dataframe to pass to the pipeline
    '''
    # NOTE: Drop all un-neded columns
    df.drop(columns=['Ticket number', 'Location', 'Violation Description', 'Latitude', 'Longitude', 'Route'],
            inplace=True, errors='ignore')

    # NOTE: Label only needed for training
    if should_add_label:
        df['is_popular_make'] = df['Make'].isin(popular_makes)
    df.drop(columns=['Make'], inplace=True, errors='ignore')

    # NOTE: Format time columns
    cols_time = ['Issue time', 'Marked Time']
    for col in cols_time:
        col_name_new = col.lower().replace('time', '').strip().replace(' ', '_')
        df[f'{col_name_new}_minute'] = df[col].dropna().astype(int).astype(str).str.slice(start=-2).apply(
            try_to_convert_to_int)
        df[f'{col_name_new}_hour'] = df[col].dropna().astype(int).astype(str).str.slice(stop=-2).apply(
            try_to_convert_to_int)
    df.drop(columns=cols_time, inplace=True, errors='ignore')
    # NOTE: Format date columns
    df['plate_expiry_year'] = df['Plate Expiry Date'].astype(str).str.slice(stop=4)
    df['plate_expiry_year'] = df['plate_expiry_year'].apply(try_to_convert_to_int).dropna().astype(int)

    # %%
    df['plate_expiry_month'] = df['Plate Expiry Date'].astype(str).str.slice(start=4, stop=6)
    df['plate_expiry_month'] = df['plate_expiry_month'].apply(try_to_convert_to_int).dropna().astype(int)

    cols_date = ['Issue Date',]
    for col in cols_date:
        col_name_new = col.lower().replace('date', '').strip().replace(' ', '_')
        df[col] = pd.to_datetime(df[col])
        df[f'{col_name_new}_day_of_month'] = df['Issue Date'].dt.day
        df[f'{col_name_new}_day_of_year'] = df['Issue Date'].dt.dayofyear
        df[f'{col_name_new}_month'] = df['Issue Date'].dt.month
        df[f'{col_name_new}_year'] = df['Issue Date'].dt.year
    df.drop(columns=cols_date, inplace=True, errors='ignore')

    # NOTE: VIN
    df['vin_make'] = df['VIN'].apply(lambda vin: VIN(vin).manufacturer)
    with open(f'{path_to_project_root}data/known_vins.json') as json_file:
        known_vins = json.load(json_file)
    df['known_vin_make'] = df['VIN'].apply(known_vins.get)
    df['is_popular_vin_make'] = df['vin_make'].apply(
        lambda val: val in popular_makes if val is not None else None)
    df.drop(columns=['VIN', 'vin_make'], inplace=True, errors='ignore')

    # NOTE: Agency
    df['Agency'] = df['Agency'].dropna().astype(int).astype(str)

    # NOTE: Violation Code
    df['violation_code'] = df['Violation code'].apply(lambda val: val if val in popular_violation_codes else 'OTHR')
    df.drop(columns=['Violation code'], inplace=True, errors='ignore')

    #NOTE: Fine amount
    df['Fine amount'] = np.log(df['Fine amount'])

    for col, category_to_int_map in category_to_int_maps.items():
        df[col] = df[col].apply(category_to_int_map.get)

    for col in numeric_features:
        df[col] = df[col].astype(float)
    return df