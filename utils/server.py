#!/usr/bin/env python3
"""
Utils around deploying a simple http server to accept JSON post requests to make predictions on an sklearn model

Based on: https://gist.github.com/mdonkers/63e115cc0c79b4f6b8b3a6b797e485c7
"""
from http.server import BaseHTTPRequestHandler
import logging
import joblib
from abc import abstractmethod
import pandas as pd
import json
from utils.data_cleaning import clean_df

model_name = 'current'
model = joblib.load(f'./models/{model_name}.joblib')
model_extra_param_info = None
with open('./models/current_info.json') as json_file:
    model_extra_param_info = json.load(json_file)
#%%
class JSON_POST_Request_Handler(BaseHTTPRequestHandler):
    '''
    Handle web POST requests expected to be in JSON format and returned in JSON format
    '''
    def _send_response(self, response):
        '''
        Send a successful response from the server
        :param response: dict: The dictionary of information to send back to the user
        '''
        self.send_response(200)
        self.send_header('Content-type', 'text/json')
        self.end_headers()
        self.wfile.write(json.dumps(response).encode('utf8'))

    def parse_post_data(self, post_data):
        '''
        Convert the posted data to a usable format
        :param post_data: str: The post data decoded to utf8 format
        :return: dict: The posted data
        '''
        try:
            return json.loads(post_data)
        except json.JSONDecodeError as e:
            # NOTE: Not a properly formatted string
            logging.error(f'Unable to parse post data body:: {e}:: {post_data}')
        except TypeError as e:
            # NOTE: Not a string
            logging.error(f'Expected post data to be a string:: {e}:: {post_data}')

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length).decode('utf-8') # <--- Gets the data itself
        logging.info(f"POST request,\nPath: {self.path}\nHeaders:\n{self.headers}\n\nBody:\n{post_data}\n")
        post_data_parsed = self.parse_post_data(post_data)
        if type(post_data_parsed) not in [list, dict]:
            return self._send_response({
                'success': False,
                'error': 'Expected JSON input'
            })
        try:
            return self._send_response({
                'success': True,
                'response': self.process_input(post_data_parsed)
            })
        except Exception as e:
            return self._send_response({
                'success': False,
                'msg': f'Error processing input: {post_data_parsed}',
                'error': str(e)
            })

    @abstractmethod
    def process_input(self, input_row):
        raise NotImplementedError('Must override in subclass')

class Prediction_Handler(JSON_POST_Request_Handler):
    '''
    Handle web requests meant to be inputs to make a prediction on a model
    '''
    def format_input_for_prediction(self, input_val):
        '''

        :param input_val: dict, list<dict>: JSON representation of either a single or many input rows
        :return: pd.DataFrame: A dataframe for all of the input predictions
        '''
        df_input = pd.DataFrame([input_val] if type(input_val) is dict else input_val)
        return clean_df(df_input, **model_extra_param_info, should_add_label=False)

    def format_prediction_for_output(self, y_pred, input_val_type):
        '''
        Ensure the output is in the desired format for the end user
        :param y_pred: list<list<float>>: The predictions for each class
        :param input_val_type: type: The type of the input value (Used to determine which format to use for output)
        :return: list<<float>, float: The probability a record's make is popular
        '''
        # NOTE: Only return for the is_popular_make class probability
        y_pred = [pred[-1] for pred in y_pred]
        # NOTE: Only return the first prediction (Should only be one input)
        return y_pred[0] if input_val_type is dict else y_pred

    def process_input(self, input_val):
        '''
        Make a prediction on the using the currently best  (unless otherwise specified) model

        :param input_row: list, dict: The JSON representation of the row(s) used to make a prediction
        :return: list<<float>: The probability a record's make is popular
        '''
        logging.info(f"Making prediction on model: {model_name}")
        df_input_clean = self.format_input_for_prediction(input_val)
        y_pred = model.predict_proba(df_input_clean).tolist()
        return self.format_prediction_for_output(y_pred, type(input_val))
