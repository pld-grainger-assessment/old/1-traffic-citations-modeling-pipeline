from string import ascii_uppercase


class VIN(object):
    def __init__(self, vin, **kwargs):
        self.kwargs = {
            **kwargs,
            **{k: v for k, v in locals().items() if k not in ['self']}
        }

    @property
    def vin(self):
        vin = self.kwargs.get('vin')
        if type(vin) is not str:
            return
        return vin.upper()

    @property
    def manufacturer_shortcode(self):
        '''
        :return: str: The shortcode sometimes indicating the manufacturer (often the 3rd digit is required as well)
        '''
        return self.vin[:2] if self.vin is not None else None

    @property
    def win(self):
        '''
        :return: str: The world identification number portion of the VIN indicating where the manufacturer is located
        '''
        return self.vin[:3] if self.vin is not None else None

    @staticmethod
    def get_vin_ord_val_for_char(c):
        '''

        :param c: The character to return an ordering value for
        :return: int: The ordering value for VIN region ordering
        '''
        ord_val = ord(c)
        # NOTE: Flip 0 and 9 ordering
        ord_map = {
            ord('0'): ord('9'),
            ord('9'): ord('0')
        }
        ord_val = ord_map.get(ord_val, ord_val)
        # NOTE: Numbers after letters
        if ord_val in range(ord('0'), ord('9')):
            ord_val += ord('Z') - ord('0') + 1

        return str(ord_val)

    @staticmethod
    def get_vin_ord_val(vin):
        '''

        :param vin: str: The value to get the ordering map
        :return: int: The ordering value of the provided string
        '''
        return int("".join([VIN.get_vin_ord_val_for_char(c) for c in vin]))

    @staticmethod
    def get_vin_country_code_range_vals(start, end):
        '''

        :param start:
        :param end:
        :return:
        '''
        return range(VIN.get_vin_ord_val(start), VIN.get_vin_ord_val(end) + 1)

    @staticmethod
    def get_region_code_map():
        '''
        :return: dict: A map of countries to the region codes which manufacturers from that country may have
        '''
        return {
            'South Africa': VIN.get_vin_country_code_range_vals('AA', 'AH'),
            'Cote d\' Ivorie': VIN.get_vin_country_code_range_vals('AJ', 'AN'),
            'unassigned_0': VIN.get_vin_country_code_range_vals('AP', 'A0'),
            'Angola': VIN.get_vin_country_code_range_vals('BA', 'BE'),
            'Kenya': VIN.get_vin_country_code_range_vals('BF', 'BK'),
            'Tanzania': VIN.get_vin_country_code_range_vals('BL', 'BR'),
            'unassigned': VIN.get_vin_country_code_range_vals('BS', 'B0'),
            'Benin': VIN.get_vin_country_code_range_vals('CA', 'CE'),
            'Madagascar': VIN.get_vin_country_code_range_vals('CF', 'CK'),
            'Tunisia': VIN.get_vin_country_code_range_vals('CL', 'CR'),
            'unassigned': VIN.get_vin_country_code_range_vals('CS', 'C0'),
            'Egypt': VIN.get_vin_country_code_range_vals('DA', 'DE'),
            'Morocco': VIN.get_vin_country_code_range_vals('DF', 'DK'),
            'Zambia': VIN.get_vin_country_code_range_vals('DL', 'DR'),
            'unassigned': VIN.get_vin_country_code_range_vals('DS', 'D0'),
            'Ethiopia': VIN.get_vin_country_code_range_vals('EA', 'EE'),
            'Mozambique': VIN.get_vin_country_code_range_vals('EF', 'EK'),
            'Ghana': VIN.get_vin_country_code_range_vals('FA', 'FE'),
            'Nigeria': VIN.get_vin_country_code_range_vals('FF', 'FK'),
            'unassigned_1': VIN.get_vin_country_code_range_vals('FL', 'F0'),
            'unassigned_2': VIN.get_vin_country_code_range_vals('GA', 'G0'),
            'unassigned_3': VIN.get_vin_country_code_range_vals('HA', 'H0'),
            'Japan': VIN.get_vin_country_code_range_vals('J', 'J'),
            'Sri Lanka': VIN.get_vin_country_code_range_vals('KA', 'KE'),
            'Israel': VIN.get_vin_country_code_range_vals('KF', 'KK'),
            'South Korea': VIN.get_vin_country_code_range_vals('KL', 'KR'),
            'Kazakhstan': VIN.get_vin_country_code_range_vals('KS', 'K0'),
            'China': VIN.get_vin_country_code_range_vals('L', 'L'),
            'India': VIN.get_vin_country_code_range_vals('ME', 'MA'),
            'Indonesia': VIN.get_vin_country_code_range_vals('MF', 'MK'),
            'Thailand': VIN.get_vin_country_code_range_vals('ML', 'MR'),
            'Myanmar': VIN.get_vin_country_code_range_vals('MS', 'M0'),
            'Iran': VIN.get_vin_country_code_range_vals('NA', 'NE'),
            'Pakistan': VIN.get_vin_country_code_range_vals('NF', 'NK'),
            'Turkey': VIN.get_vin_country_code_range_vals('RL', 'NR'),
            'unassigned_4': VIN.get_vin_country_code_range_vals('NS', 'N0'),
            'Philippines': VIN.get_vin_country_code_range_vals('PA', 'PE'),
            'Singapore': VIN.get_vin_country_code_range_vals('PF', 'PK'),
            'Malaysia': VIN.get_vin_country_code_range_vals('PL', 'PR'),
            'unassigned_5': VIN.get_vin_country_code_range_vals('PS', 'P0'),
            'United Arab Emirates': VIN.get_vin_country_code_range_vals('RA', 'RE'),
            'Taiwan': VIN.get_vin_country_code_range_vals('RF', 'RK'),
            'Vietnam': VIN.get_vin_country_code_range_vals('RL', 'RR'),
            'Saudi Arabia': VIN.get_vin_country_code_range_vals('RS', 'R0'),
            'United Kingdom': VIN.get_vin_country_code_range_vals('SA', 'SM'),
            'Germany (east)': VIN.get_vin_country_code_range_vals('SN', 'ST'),
            'Poland': VIN.get_vin_country_code_range_vals('SU', 'SZ'),
            'Latvia': VIN.get_vin_country_code_range_vals('S1', 'S4'),
            'unassigned_6': VIN.get_vin_country_code_range_vals('S5', 'S0'),
            'Switzerland': VIN.get_vin_country_code_range_vals('TA', 'TH'),
            'Czech Republic': VIN.get_vin_country_code_range_vals('TJ', 'TP'),
            'Hungary': VIN.get_vin_country_code_range_vals('TR', 'TV'),
            'Portugal': VIN.get_vin_country_code_range_vals('TW', 'T1'),
            'unassigned_7': VIN.get_vin_country_code_range_vals('T2', 'T0'),
            'unassigned_8': VIN.get_vin_country_code_range_vals('UA', 'UG'),
            'Denmark': VIN.get_vin_country_code_range_vals('UH', 'UM'),
            'Ireland': VIN.get_vin_country_code_range_vals('UN', 'UT'),
            'Romania': VIN.get_vin_country_code_range_vals('uu', 'uz'),
            'unassigned_9': VIN.get_vin_country_code_range_vals('U1', 'U4'),
            'Slovakia': VIN.get_vin_country_code_range_vals('U5', 'U7'),
            'unassigned_10': VIN.get_vin_country_code_range_vals('U8', 'U10'),
            'Austria': VIN.get_vin_country_code_range_vals('VA', 'VE'),
            'France': VIN.get_vin_country_code_range_vals('VF', 'VR'),
            'Spain': VIN.get_vin_country_code_range_vals('VS', 'VW'),
            'Serbia': VIN.get_vin_country_code_range_vals('VX', 'V2'),
            'Croatia': VIN.get_vin_country_code_range_vals('V3', 'V5'),
            'Estonia': VIN.get_vin_country_code_range_vals('V6', 'V0'),
            'Germany (west)': VIN.get_vin_country_code_range_vals('W', 'W'),
            'Bulgaria': VIN.get_vin_country_code_range_vals('XA', 'XE'),
            'Greece': VIN.get_vin_country_code_range_vals('XF', 'XK'),
            'Netherlands': VIN.get_vin_country_code_range_vals('XL', 'XR'),
            'Russia (USSR)': VIN.get_vin_country_code_range_vals('XS', 'XW'),
            'Luxembourg': VIN.get_vin_country_code_range_vals('XX', 'X2'),
            'Russia': VIN.get_vin_country_code_range_vals('X3', 'X0'),
            'Belgium': VIN.get_vin_country_code_range_vals('YA', 'YE'),
            'Finland': VIN.get_vin_country_code_range_vals('YF', 'YK'),
            'Malta': VIN.get_vin_country_code_range_vals('YL', 'YR'),
            'Sweden': VIN.get_vin_country_code_range_vals('YS', 'YW'),
            'Norway': VIN.get_vin_country_code_range_vals('YX', 'Y2'),
            'Belarus': VIN.get_vin_country_code_range_vals('Y3', 'Y5'),
            'Ukraine': VIN.get_vin_country_code_range_vals('Y6', 'Y0'),
            'Italy': VIN.get_vin_country_code_range_vals('ZA', 'ZR'),
            'unassigned_11': VIN.get_vin_country_code_range_vals('ZS', 'ZW'),
            'Slovenia': VIN.get_vin_country_code_range_vals('ZX', 'Z2'),
            'Lithuania': VIN.get_vin_country_code_range_vals('Z3', 'Z5'),
            'unassigned_12': VIN.get_vin_country_code_range_vals('Z6', 'Z0'),
            'United States 1': VIN.get_vin_country_code_range_vals('1', '1'),
            'United States 2': VIN.get_vin_country_code_range_vals('4', '4'),
            'United States 3': VIN.get_vin_country_code_range_vals('5', '5'),
            'Canada': VIN.get_vin_country_code_range_vals('2', '2'),
            'Mexico': VIN.get_vin_country_code_range_vals('3A', '3W'),
            'Costa Rica': VIN.get_vin_country_code_range_vals('3X', '37'),
            'Cayman Islands': VIN.get_vin_country_code_range_vals('38', '39'),
            'unassigned_13': VIN.get_vin_country_code_range_vals('30', '30'),
            'Australia': VIN.get_vin_country_code_range_vals('6', '6'),
            'New Zealand': VIN.get_vin_country_code_range_vals('7', '7'),
            'Argentina': VIN.get_vin_country_code_range_vals('8A', '8E'),
            'Chile': VIN.get_vin_country_code_range_vals('8F', '8K'),
            'Ecuador': VIN.get_vin_country_code_range_vals('8L', '8R'),
            'Peru': VIN.get_vin_country_code_range_vals('8S', '8W'),
            'Venezuela': VIN.get_vin_country_code_range_vals('8X', '82'),
            'unassigned_14': VIN.get_vin_country_code_range_vals('83', '80'),
            'Brazil 1': VIN.get_vin_country_code_range_vals('9A', '9E'),
            'Colombia': VIN.get_vin_country_code_range_vals('9F', '9K'),
            'Paraguay': VIN.get_vin_country_code_range_vals('9L', '9R'),
            'Uruguay': VIN.get_vin_country_code_range_vals('9S', '9W'),
            'Trinidad & Tobago': VIN.get_vin_country_code_range_vals('9X', '92'),
            'Brazil 2': VIN.get_vin_country_code_range_vals('93', '99'),
            'unassigned_15': VIN.get_vin_country_code_range_vals('90', '90'),
        }

    @property
    def region_code(self):
        '''

        :return: str: The portion of the vin used to indicated the general region of the manufacturer
        '''
        if self.vin is None:
            return
        if len(self.vin) < 1:
            return
        return self.vin[:2] if self.vin[0] not in ['J', 'L', '1', '2', '4', '5', '6', '7'] else self.vin[0]

    @property
    def region_code_ord_val(self):
        '''
        :return: int: The ordering value of the region code
        '''
        return self.get_vin_ord_val(self.region_code)

    @property
    def region(self):
        '''
        :return: str?: The region of the world the vehicle is from
        '''
        if self.vin is None:
            return
        for region, region_codes in VIN.get_region_code_map():
            if self.region_code_ord_val in region_codes:
                return region

    @property
    def does_vehicle_year_use_1980_range(self):
        '''

        :return: bool?: Whether the VIN uses the year ranging starting with 1980 for indicating the model year
        '''
        if len(self.vin) < 7:
            return None
        return self.vin[6] not in ascii_uppercase

    @property
    def vin_year_ord_to_vehicle_year_alpha_delta(self):
        '''

        :return: int: the amount to add to the year ordering character to get the correct model year
        '''
        return (1980 if self.does_vehicle_year_use_1980_range else 2010) - ord('A')

    @property
    def vin_year_ord_to_vehicle_year_numeric_delta(self):
        '''

        :return: int: the amount to add to the year ordering character to get the correct model year
        '''
        return (2001 if self.does_vehicle_year_use_1980_range else 2031) - ord('1')

    @property
    def vehicle_year(self):
        '''
        In order to identify the exact year in passenger cars and multipurpose passenger vehicles with a GVWR
        of 10,000 or less, one must read position 7 as well as position 10.

        For passenger cars, and for multipurpose passenger vehicles and trucks with a gross vehicle weight rating
        of 10,000 lb (4,500 kg) or less, if position seven is numeric, the model year in position 10 of the
        VIN refers to a year in the range 1980–2009.[citation needed]

        If position seven is alphabetic, the model year in position 10 of VIN refers to a year in the range 2010–2039.

        :return: int?: The vehicle year
        '''
        if len(self.vin) < 10:
            return None
        return ord(self.vin[9]) + (self.vin_year_ord_to_vehicle_year_alpha_delta if self.vin[6] in ascii_uppercase else self.vin_year_ord_to_vehicle_year_numeric_delta)

    @property
    def common_manufacturer_win_map(self):
        '''

        :return: dict: A map of the manufacturer to the WMI codes that the belong to them
        '''
        return {

            'TOYT': [
                'AHT',  # South Africa
                'JT',  # South Africa
                'LFM',  # China - FAW
                'LTV',  # China - FAW - Tianjin
                'LVG',  # China - GAC
                'NMT',  # Turkey
                'PN1',  # Malaysia
                'SB1',  # UK
                'VNK',  # France
                '2T',  # Canada
                '4T',  # US
                '4T',  # US
                '6T1',  # Australia
                '7A4',  # New Zealand
                '8AJ',  # Argentina
                '9BR',  # Brazil
            ],
            'HOND': [],
            'FORD': [
                'AFA', # South Africa
                'LVS', # China - Changan
                'NM0', # Turkey - Otosan
                'PR8', # Malaysia
                'WF0', # Germany
                '1F', # United States
                '1ZV', # United States
                '2F', # Canada
                '3F', # Mexico
                '6F', # Australia
                '7A5', # New Zealand
                '8AF', # Argentina
            ],
            'NISS': [
                'JN',
                'MDH', # India
                'MNT', # Thailand
                'SJN', # UK
                'VNV', # Renault-Nissan
                'VSK', # Spain
                'VWA', # Spain
                '1N', # USA
                '3N', # Mexico
                '5N1', # USA
                '6F4', # Australia
                '94D', # Brazil
            ],
            'CHEV': [
                'MMM', # Thailand
                '1GC', # US Truck
                '1G1', # US
                '2G1', # Canada
                '8AG', # Argentina
                '8GG', # Chile
                '8LD', # Ecuador
                '9BG', # Brazil
            ],
            'BMW': [
                'WBA',
                'WBS',
                'WBW',
                'WBY',
                '4US',
                'WB1'
            ],
            'MERZ': [
                'MBR', #India
                'NLE', # Turk Truck
                'NMB', # Turk Buses
                'VSA', # Spain
                'WDB', # WDB
                'WDD',
                'WDF',
                'WMX',
                '4JG', # USA
                '9BM', # Brazil
            ],
            'VOLK': [
                'AAV',  # South Africa
                'WVG',  # Germany
                'WVW',  # Germany
                'WV1',  # Germany - Commercial
                'WV2',  # Germany - Commercial
                'LFV',  # China - FAW
                '1VW',  # United States
                '3VW',  # Mexico
                '8AW',  # Argentina
                '9BW',  # Brazil
                '953',  # VW Trucks / MAN
            ],
            'HYUN': [
                'AC5', # South Africa
                'ADD', # South Africa
                'KM',
                'LBE', # Beijing
                'MAL',
                'NLH', # Assan
                'TMA', # Czech
                'X7M', # Russia
                '2HM', # Canada
                '5NP', # USA
            ],
            'DODG': [
                '1B3',
                '1D3',
                '2B3', # Canada
                '2B7', # Canada
                '2D3', # Canada
                '3D3', # Mexico
                '3D4', # Mexico
            ],
            'LEXS': [

            ],
            'KIA': [
                'U5Y', # Slovakia
                'U6Y', # Slovakia
                'KN', #
                'MS0', # Myanmar
            ],
            'JEEP': [
                '1J4', #
                '1J8', #
            ],
            'AUDI': [
                'TRU',
                'WAU',
                'WA1',
                'WUA',
                '93U',
                '93V',
            ],
            'MAZD': [
                'JM1',
                'JMZ',
                'MM8',
                'PE3',
                'YCM', # Belgium
                '1YV', # USA
                '3MD', # Mexico
                '3MZ', # Mexico
            ],
            'GMC': [
                'KL', # South Korea
                'LSG', # China
                'SED', # Luton
                'XUF', # Russia
                '1G', # USA
                '2GX', # Canada
                '3G', # Mexico
                '6G1', # Holden
                '6H8', # Holden
            ],
            'CHRY': [
                'WDC', #
                '1C3', #
                '1C4', #
                '1C6', #
                '2A4', # Canada
                '2C3', # Canada
                '3C4', # Mexico
                'WDC', # Daimler
            ],
            'INFI': [

            ],
            'ACUR': [],
            'SUBA': [
                'JF', # Fuji Heavy Industries
                '4S', # Isuzu Automotive
            ],
            'VOLV': [
                '4V1', #
                '4V2', #
                '4V3', #
                '4V4', #
                '4V5', #
                '4V6', #
                '4VL', #
                '4VM', #
                '4VZ', #
                'MC2', #
                'XLB', #
                'YB1', #
                'YV1', #
                'YV2', #
                'YV3', #
                'YV4', #
            ],
            'TOYO': [],
            'MITS': [
                'JA3',
                'JA4',
                'JL5',
                'JMB',
                'JMY',
                'MA7',
                'MMB',
                'MMC',
                'MMT',
                'TYA',
                'TYB',
                'XMC',
                '6MM',
                '93X'
            ],
            'CADI': [],
        }

    @property
    def manufacturer(self):
        '''
        The make may only be identified after looking at positions one through three and another position, as determined by the manufacturer in the second section or fourth to eighth segment of the VIN.

        :return:
        '''
        if self.vin is None:
            return
        for manufacturer, short_code_or_win_list in self.common_manufacturer_win_map.items():
            if self.manufacturer_shortcode in short_code_or_win_list or self.win in short_code_or_win_list:
                return manufacturer

    @property
    def is_valid_checksum(self):
        '''
        Validate a VIN against the 9th position checksum

        :param val: str: The text to be validated as a vin
        :return: bool: Whether the provided value is potentially a valid vin
        '''
        """
        See: http://en.wikipedia.org/wiki/Vehicle_Identification_Number#Check_digit_calculation
        Credit: https://gist.github.com/krisneuharth/2176789
    
        Test VINs:
            1M8GDM9AXKP042788
            11111111111111111
        """
        if self.vin is None:
            return
        POSITIONAL_WEIGHTS = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2]
        ILLEGAL_ALL = ['I', 'O', 'Q']
        ILLEGAL_TENTH = ['U', 'Z', '0']
        LETTER_KEY = dict(
            A=1, B=2, C=3, D=4, E=5, F=6, G=7, H=8,
            J=1, K=2, L=3, M=4, N=5, P=7, R=9,
            S=2, T=3, U=4, V=5, W=6, X=7, Y=8, Z=9,
        )

        if len(self.vin) == 17:
            for char in ILLEGAL_ALL:
                if char in self.vin:
                    return False
                    raise ValueError('Val cannot contain "I", "O", or "Q".')

            if self.vin[10] in ILLEGAL_TENTH:
                return False
                raise ValueError('Val cannot contain "U", "Z", or "0" in position 10.')

            check_digit = self.vin[8]

            pos = sum = 0
            for char in self.vin:
                value = int(LETTER_KEY[char]) if char in LETTER_KEY else int(char)
                weight = POSITIONAL_WEIGHTS[pos]
                sum += (value * weight)
                pos += 1

            calc_check_digit = int(sum) % 11

            if calc_check_digit == 10:
                calc_check_digit = 'X'

            if str(check_digit) != str(calc_check_digit):
                return False
                raise ValueError('Invalid VIN.')
        else:
            return False
            raise ValueError('Val must be 17 characters.')
        return True

is_valid_vin = lambda val: VIN(vin=val).is_valid_checksum