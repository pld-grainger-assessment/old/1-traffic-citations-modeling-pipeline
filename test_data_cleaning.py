import pytest
from utils.data_cleaning import clean_df
from tests.utils.test_case_runner import Test_Case_Runner
from tests.cases.data_cleaning import input_rows
import json
import pandas as pd


class Test_Results(Test_Case_Runner):
    @pytest.fixture
    def df(self):
        df = pd.DataFrame(input_rows)
        yield df

    @pytest.fixture
    def clean_data_params(self):
        with open('./models/current_info.json') as json_file:
            model_extra_param_info = json.load(json_file)
        yield model_extra_param_info

    def test_clean_data_params(self, clean_data_params):
        assert type(clean_data_params) is dict
        for key in ['popular_makes', 'popular_violation_codes', 'numeric_features', 'category_to_int_maps']:
            assert key in clean_data_params
        assert type(clean_data_params['popular_makes']) is list
        assert type(clean_data_params['popular_violation_codes']) is list
        assert type(clean_data_params['numeric_features']) is list
        assert type(clean_data_params['category_to_int_maps']) is dict

    def test_clean_df(self, df, clean_data_params):
        '''


        case: dict<str:Any>: The case to test
        '''
        df_len = len(df)
        df_cleaned = clean_df(df, **clean_data_params)
        assert type(df_cleaned) is pd.DataFrame
        assert df_len == len(df_cleaned)
        assert 'Make' not in df_cleaned.columns

        clean_df
