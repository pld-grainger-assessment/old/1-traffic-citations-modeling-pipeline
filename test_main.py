import pytest
from main import run
from tests.utils.test_case_runner import Test_Case_Runner
from tests.cases.main import cases
import socket, errno
import requests
import threading
import json
import ctypes


class Server_Thread(threading.Thread):
    def __init__(self, name, port):
        threading.Thread.__init__(self)
        self.name = name
        self.port = port

    def run(self):
        # target function of the thread class
        try:
            run(port=self.port)
        finally:
            print('ended')

    def get_id(self):

        # returns id of the respective thread
        if hasattr(self, '_thread_id'):
            return self._thread_id
        for id, thread in threading._active.items():
            if thread is self:
                return id

    def raise_exception(self):
        thread_id = self.get_id()
        print(f'Thread id: {thread_id}')
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(thread_id),
                                                         ctypes.py_object(KeyboardInterrupt))
        print(f"res: {res}")
        if res == 0:
            raise ValueError("Invalid thread ID")
        elif res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')

class Test_Results(Test_Case_Runner):
    @pytest.fixture
    def server_for_testing(self):
        import time
        self.port = 8081
        server_for_testing = Server_Thread('Test Server', port=self.port)
        server_for_testing.start()
        time.sleep(1)
        yield server_for_testing
        server_for_testing.raise_exception()
        time.sleep(1)

    def test_main(self, server_for_testing):
        '''
        Run the tests for this question against the main method

        case: dict<str:Any>: The case to test
        '''

        # NOTE: Run in a subprocess

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.bind(("127.0.0.1", self.port))
            s.close()
            assert False #NOTE: Expect port to be bound
        except socket.error as e:
            assert e.errno == errno.EADDRINUSE
        finally:
            print('Raise Exception')

    @pytest.mark.parametrize("case", cases)
    def test_post_request(self, case, server_for_testing):
        '''
        Test that the server accepts POST requests after being started

        :param case:  dict: The current case to test for making a POST request against the local server
        '''# NOTE: Run in a subprocess
        import time
        response = requests.post(f"http://127.0.0.1:{self.port}", data=json.dumps(case.get('inputs')))
        assert response.status_code == 200
        data = response.json()
        assert data is not None
        assert 'success' in data
        assert data['success'] == case['expect_success']
        if data['success']:
            assert 'response' in data
        else:
            assert 'error' in data
