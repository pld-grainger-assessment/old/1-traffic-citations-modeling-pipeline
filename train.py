'''
Run the training to save the current best model.

Usage:
    python train.py
'''
# %%

from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.decomposition import TruncatedSVD
from sklearn.metrics import balanced_accuracy_score, make_scorer, confusion_matrix
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from matplotlib import pyplot as plt
from sklearn.externals import joblib
import pandas as pd
import json
from utils.data_cleaning import clean_df

# %% md

## Load Data

# %%
print('Loading Data..')
url = 'https://s3-us-west-2.amazonaws.com/pcadsassessment/parking_citations.corrupted.csv'
df = pd.read_csv(url)

# %%

df_with_make = df[~df['Make'].isnull()]

# %% md

## Setup Hash Maps

# %%

# NOTE: Get Popular Makes
print('Setting up Popular Makes..')
s_num_citations_per_make = df_with_make[df_with_make['Make'] != 'OTHR']['Make'].value_counts()
num_popular_makes = 25
popular_makes = s_num_citations_per_make[:num_popular_makes].keys().tolist()

# %%

# NOTE: Get Popular Citation Numbers
print('Setting up Popular Citation Numbers..')
num_popular_violation_codes = 22
num_records_per_violation_code = df['Violation code'].value_counts().sort_values(ascending=False)
popular_violation_codes = num_records_per_violation_code[:num_popular_violation_codes].keys().tolist()

# %%

df_with_make['violation_code'] = df_with_make['Violation code'].apply(
    lambda val: val if val in popular_violation_codes else 'OTHR')

# %% md

## Prep for one-hot encoding

# %%
print('Setting up category to unique number map for one hot encoding..')
categorical_features = ['violation_code', 'RP State Plate', 'Agency', 'Meter Id', 'Color', 'Body Style']
category_to_int_maps = {
    col: {
            key: index
            for index, key in enumerate(df_with_make[col].value_counts().keys())
        }
    for col in categorical_features
}

for col in categorical_features:
    category_to_int_map = {
        key: index
        for index, key in enumerate(df_with_make[col].value_counts().keys())
    }
    df_with_make[col] = df_with_make[col].apply(category_to_int_map.get)

#%%
numeric_features = ['Fine amount', 'issue_minute', 'issue_hour', 'marked_minute', 'marked_hour', 'issue_day_of_month', 'issue_day_of_year',]#'
numeric_features += ['issue_month', 'issue_year', 'plate_expiry_month', 'plate_expiry_year']
# %% md

## Split Data

# %%
print('Splitting to labeled data..')
df_with_make = df[~df['Make'].isnull()]
df_with_make = clean_df(df_with_make, popular_makes, popular_violation_codes, numeric_features, category_to_int_maps,
                        should_add_label=True)
print('Saving model associated info..')
with open('./models/current_info.json', 'w') as outfile:
    json.dump(dict(popular_makes=popular_makes, popular_violation_codes=popular_violation_codes, numeric_features=numeric_features, category_to_int_maps=category_to_int_maps), outfile)
#%%
# NOTE: As these are created in the cleaning process and should be numeric already they do not need to be prepped for one-hot encoding
# NOTE: Model seems better when treating these as numeric..
# categorical_features += ['issue_month', 'issue_year', 'plate_expiry_month', 'plate_expiry_year']
# %%
print('Splitting dataa to train/test..')

X = df_with_make[[col for col in df_with_make.columns if col not in ['Make', 'is_popular_make']]]
Y = df_with_make['is_popular_make']
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=47)

# %% md

## Setup Classifier Pipeline

# %%
print('Setting up pipeline..')
numeric_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='median')),
    ('scaler', StandardScaler())
])

categorical_transformer = Pipeline(steps=[
    ('imputer', SimpleImputer(strategy='constant', fill_value=-1)),
    ('onehot', OneHotEncoder(handle_unknown='ignore'))])

preprocessor = ColumnTransformer(
    transformers=[
        ('num', numeric_transformer, numeric_features),
        ('cat', categorical_transformer, categorical_features)
    ],
    # remainder='passthrough'
)

# %%

pipeline = Pipeline([
    ('preprocessor', preprocessor),
    ('pca', TruncatedSVD()),
    ('clf', LogisticRegression())
])
parameters = {
    'pca__n_components': [5, 15, 45, len(X_train.columns)],  # ,10,20,50
}

# NOTE: Setup many pipelines with same base but different classifiers
# TODO: Would be more efficient to switch between classifiers in last step of pipeline so as not to repeat base steps


grid = GridSearchCV(pipeline, param_grid=parameters,scoring=make_scorer(balanced_accuracy_score))#, cv=2
# %%

get_model_path = lambda model_name: f'./models/{model_name}.joblib'

def print_grid_results(grid):
    print(f"Model score: {grid.score(X_test, y_test)}")
    print(f"Best Params: {grid.best_params_}")
    y_pred = grid.predict(X_test)
    labels = ['Not Popular', 'Popular']
    conf_mat = pd.DataFrame(confusion_matrix(y_true=y_test, y_pred=y_pred), columns=labels, index=labels)
    print(f'Accuracy: {(y_pred == y_test).sum() / len(y_pred)}')
    print(f"Frequency model predicted True: {len(y_pred[y_pred == True]) / len(y_pred)}")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(conf_mat / len(y_pred), cmap=plt.cm.Blues)
    fig.colorbar(cax)
    ax.set_xticklabels([''] + labels)
    ax.set_yticklabels([''] + labels)
    plt.xlabel('Predicted')
    plt.ylabel('Expected')
    print(f'Saving model confusing matrix figures..')
    plt.savefig('./data/current_grid_conf_matrix.png')
    conf_mat.to_csv('./data/current_grid_conf_matrix.csv')
    print(conf_mat / len(y_pred))

#%%
class_weights = {
    True: .4,
    False: .6
}
sample_weights = y_train.apply(class_weights.get)
#%%
# %%

# NOTE: Train best classifier
print(f'Training model using: {grid}')
grid.fit(X_train, y_train, clf__sample_weight=sample_weights)
model_path = get_model_path('current')
print(f'Saving model to: {model_path}')
joblib.dump(grid, model_path)
print_grid_results(grid)