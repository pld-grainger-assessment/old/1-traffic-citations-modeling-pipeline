#!/usr/bin/env python3
"""
Very simple HTTP server in python for making predictions to an sklearn model trained to predict whether a vehicle
citation is for vehicle with a popular make

Based on: https://gist.github.com/mdonkers/63e115cc0c79b4f6b8b3a6b797e485c7

Usage::
    python  main.py [--port <port>]
"""

import logging
from http.server import HTTPServer
from utils.server import Prediction_Handler

def run(port=8080, server_class=HTTPServer, handler_class=Prediction_Handler):
    '''
    Start the server
    :param port: int: The port to start the server
    :param server_class: HTTPServer: The class to start the server
    :param handler_class: BaseHTTPRequestHandler: Which class should handle the request
    :return:
    '''
    logging.basicConfig(level=logging.INFO)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logging.info('Starting httpd...\n')

    print('SERVING!!!')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    except SystemExit:
        pass
    except Exception as e:
        print(f'Got error: {e}: {type(e)}')
    httpd.server_close()
    logging.info('Stopping httpd...\n')

if __name__ == '__main__':
    from argparse import ArgumentParser
    arg_parser = ArgumentParser()
    arg_parser.add_argument('port', nargs='?', action='store',  default=8080)
    args = vars(arg_parser.parse_args())
    run(port=args['port'])